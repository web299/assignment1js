document.addEventListener("DOMContentLoaded", main);
//The main function is the place where everything comes together. It is ran when the page is loaded.
function main() {
  createFormElement(objectLiteralsMaker());
  drawTable(objectLiteralsMaker());
  tableChanger();
}

//The below function stores all the attributes of the inputs into different object literals depending on their nature. It returns an array of 
//object literals so that it can be used in the createFormElement(param) which will be later be used to make the input elements.
function objectLiteralsMaker() {
  const row_count = { type: "text", id: "row_count", size: "2", value: "2" };
  const coloumn_count = {
    type: "text",
    id: "col_count",
    size: "2",
    value: "3",
  };
  const table_width = {
    type: "number",
    id: "table_width",
    size: "3",
    value: "100",
    max: "100",
    min: "2",
  };
  const text_color = {
    type: "color",
    id: "text_color",
    size: "7",
    value: "#000000",
  };
  const background_color = {
    type: "color",
    id: "background_color",
    size: "7",
    value: "#FFFFFF",
  };
  const border_color = {
    type: "color",
    id: "border_color",
    size: "7",
    value: "#000000",
  };
  const border_width = {
    type: "text",
    id: "border_width",
    size: "2",
    value: "1",
  };
  //This is the array which will be returned. All the pervios variables are stores at once in the below array.
  const inputArray = [
    row_count,
    coloumn_count,
    table_width,
    text_color,
    background_color,
    border_color,
    border_width,
  ];
  return inputArray;
}

//This fucntion gets as input an array which will be used to make all the input elements with their attributes. The array which
//is used in this function comes from the function  objectLiteralsMaker().
function createFormElement(param) {
  //myArray will store all the input elements in it later in the program.The reason for this is because the 'fieldsetMaker()'
  //takes an array as input and creates a fieldset for it with a legend. By giving 'myArray'  as its parameter, we can make a fieldset for 
  //each input element with its legend.
  const myArray = [];
  let form = document.getElementsByTagName("form")[0];
  //This 'for' loop goes through the parameter, with it, it creates a second array called 'keys' where the values are the keys of the object literals. Using them,
  //it sets the attributes of each input element.
  for (let x = 0; x < param.length; x++) {
    let input = document.createElement("input");
    const keys = Object.keys(param[x]);
    for (let i = 0; i < keys.length; i++) {
      input.setAttribute(keys[i], param[x][keys[i]]);
    }
    //inputs are added to this array one by one
    myArray.push(input);
  }
  fieldsetMaker(myArray);
}

//This function simply makes a fieldset for all the inputs. 
function fieldsetMaker(inputElement) {
  //This is an array of the later used legends. These legends are used to make a title for each fieldset.
  let legendArray = [
    "row_count",
    "coloumn_count",
    "table_width",
    "text_color",
    "background_color",
    "border_color",
    "border_width",
  ];
  //Everything is done in the code below. 'legendarray' values are added to the 'legend' element;'legend' is added to the fieldset and finally, fieldset is add to the
  //form.
  let form = document.getElementsByTagName("form")[0];
  for (let i = 0; i < inputElement.length; i++) {
    let fieldset = document.createElement("fieldset");
    let legend = document.createElement("legend");
    legend.innerText = legendArray[i];
    fieldset.appendChild(legend);
    fieldset.appendChild(inputElement[i]);
    form.appendChild(fieldset);
  }
}

//This is the function that creates the table itseld; It takes as input an array of object literals. Using its values, it generates a
//table. Without the events, this function is not responsive, which means all the values used to generate the table are default values.
//Later using eventListeners, this generation becomes dynamic, meaning that the user can decide all necessary values used to make the table.
function drawTable(inputArray) {
  let tabel_space = document.createElement("table");
  let table_body = document.createElement("tbody");
  document.getElementById("table-render-space").appendChild(tabel_space);
  document.getElementsByTagName("table")[0].appendChild(table_body);

  //The below 'for' loop makes the row.
  for (let row_counter = 0; row_counter < inputArray[0].value; row_counter++) {
    const table_row = document.createElement("tr");
    document.getElementsByTagName("tbody")[0].appendChild(table_row);
  }
  //The below 'for' loop creates the coloumns. This is a nested 'for' loop since for each row, we have to make multiple coloumns.
  for (
    let coloumn_counter = 0;
    coloumn_counter < inputArray[1].value;
    coloumn_counter++
  ) {
    for (let i = 0; i < document.getElementsByTagName("tr").length; i++) {
      const table_coloumn = document.createElement("td");
      document.getElementsByTagName("tr")[i].appendChild(table_coloumn);
    }
  }
  //The below 'for' loop adds the text which is inside every cell. First digit represents the row number and the second digit represents the coloumn number.
  for (let row = 0; row < document.getElementsByTagName("tr").length; row++) {
    for (
      let coloumn = 0;
      coloumn <
      document.getElementsByTagName("tr")[row].querySelectorAll("td").length;
      coloumn++
    ) {
      let innerText = document.createTextNode("cell" + row + coloumn);
      document
        .getElementsByTagName("tr")
        [row].querySelectorAll("td")
        [coloumn].appendChild(innerText);
    }
  }
  //myString is the code which created the table. It gets its value from another function called 'codeFormatter'.
  //myString gets appended to the textarea so we can see the code that created the table.
  let myString = codeFormatter();
  let textArea = document.createElement("textarea");
  document.getElementById("table-html-space").appendChild(textArea);
  document.getElementsByTagName("textarea")[0].innerHTML = myString;

  //The below lines style the table.
  document.getElementsByTagName("table")[0].style.width =
    inputArray[2].value + "%";
  document.getElementsByTagName("table")[0].style.color = inputArray[3].value;
  document.getElementsByTagName("table")[0].style.backgroundColor =
    inputArray[4].value;
  document.getElementsByTagName("table")[0].style.borderColor =
    inputArray[5].value;
  document.getElementsByTagName("table")[0].style.borderWidth =
    inputArray[6].value + "px";
  for(let i=0;i<document.getElementsByTagName("td").length;i++){
    document.getElementsByTagName("td")[i].style.borderColor=inputArray[5].value;
    document.getElementsByTagName("td")[i].style.borderWidth=inputArray[6].value+"px";
  }
}
//This function holds all the eventListeners in it. These events are triggerd everytime the user changes a value in the form. This function also delets the
//ex table and the ex presented code. After, it creates a new table using the inputs which came from the user. 
function tableChanger() {
  let inputArray = objectLiteralsMaker();
  //ids is an array which holds all the ids(input ids) except the first input since the first input is directly
  //accessd to put the keyup event on it.
  let ids = [];
  for (let i = 1; i < inputArray.length; i++) {
    ids[i - 1] = inputArray[i].id;
  }
  //This is the addEventListener for the keyup event. I decided to use this event on the row_count input element.
  document.getElementById("row_count").addEventListener("keyup", (e) => {
    inputArray[0].value = e.target.value;
    document.getElementById("table-render-space").innerHTML = "";
    document.getElementById("table-html-space").innerHTML = "";
    drawTable(inputArray);
  });
  //The other input elements get the change event on them. Whenever their value is changed, this event is triggerd and a corresponding table is generated.
  for (let i = 1; i < document.getElementsByTagName("input").length; i++) {
    document
      .getElementsByTagName("input")
      [i].addEventListener("change", (e) => {
        for (let x = 0; x < ids.length; x++) {
          if (ids[x] == e.currentTarget.id) {
            inputArray[x + 1].value = e.target.value;
          }
        }
        document.getElementById("table-render-space").innerHTML = "";
        document.getElementById("table-html-space").innerHTML = "";
        drawTable(inputArray);
      });
  }
}
//This function creates the String which will be displayed in the textarea. It is used in the 'drawTable()' function
// to change the presented code.
function codeFormatter() {
  //myString holds the string representing the code.
  let myString = "<table>";
  myString = myString + "\n\t";
  myString = myString + "<tbody>";

  let numRows = document.getElementsByTagName("tr").length;
  let numColoumns = document.getElementsByTagName("tr")[0].childNodes.length;
  for (let i = 0; i < numRows; i++) {
    myString = myString + "\n\t\t";
    myString = myString + "<tr>";

    for (let x = 0; x < numColoumns; x++) {
      myString = myString + "\n\t\t\t";
      myString =
        myString +
        "<td>" +
        document.getElementsByTagName("tr")[i].querySelectorAll("td")[x]
          .innerText +
        "</td>\n";
    }
    myString = myString + "\n\t\t";
    myString = myString + "</tr>";
  }
  myString = myString + "\n\t";
  myString = myString + "</tbody>\n";
  myString = myString + "</table>";
  return myString;
}
